import axios from "axios";

const bankApi = axios.create({
    baseURL: 'https://bank.gov.ua/NBUStatService/v1/statdirectory',
     headers: {'Content-Type': 'application/json'}
  });

  class ApiService {
    fetchCurrencies() {
      return axios.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json')
        .then(res => res.data)
        .catch((error) => {
          console.log(error);
        });
    }

    async getCurrencyRate() {
      const fetchedCurrencies = await this.fetchCurrencies();

      const currenscyRate = { UAH: "1" };
      fetchedCurrencies.forEach(el => currenscyRate[el.cc] = el.rate);
      return currenscyRate;
    }
  }

  export default new ApiService();