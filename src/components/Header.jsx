import React from 'react'
import styles from './Header.module.css'


const Header = ({currenscies}) => {
const USD = currenscies.USD.toFixed(2);
const EUR = currenscies.EUR.toFixed(2);

  return (

   <header className={styles.Header}>
       <span>USD = {USD}</span>
       <span>EUR = {EUR}</span>
   </header>
  )
}

export default Header