import React, { useState } from 'react'
import CustomInput from './CustomInput'
import CustomSelect from './CustomSelect'
import styles from './CustomForm.module.css';

const CustomForm = ({currenscies}) => {
    const [currenscy1, setCurrenscy1] = useState(0);
    const [currenscy2, setCurrenscy2] = useState(0);
    const [customSelect1, setCustomSelect1] = useState('UAH');
    const [customSelect2, setCustomSelect2] = useState('UAH');
   
    const changeValueCustomSelect1 = (e) => {
    if(customSelect1 === customSelect2){
        setCurrenscy1(e);
       setCurrenscy2(e);
    }else{
        setCurrenscy1(e);
       let val1 = currenscies[customSelect1];
       let val2 = currenscies[customSelect2];
        let res = val1 / val2 * e;

        console.log(val1, val2);
        setCurrenscy2(res.toFixed(4));
    }

    }
    const changeValueCustomSelect2 = (e) => {
        if(customSelect1 === customSelect2){
            setCurrenscy1(e);
           setCurrenscy2(e);
        }else{
            setCurrenscy2(e);
            let val1 = currenscies[customSelect1];
            let val2 = currenscies[customSelect2];
             let res = val2 / val1 * e;
             setCurrenscy1(res.toFixed(4));
        }
    }

    const changeSelectItem1 = (e) =>{
        setCustomSelect1(e);
        changeValueCustomSelect1(currenscy1);
    }
    const changeSelectItem2 = (e) =>{
        setCustomSelect2(e);
        changeValueCustomSelect2(currenscy2);
    }

    return (
    <form className={styles.CustomForm}>
        <div>
        <CustomSelect currenscies={currenscies} customSelect={customSelect1} setCustomSelect={changeSelectItem1}/>
        <CustomInput currenscy={currenscy1} setCurrenscy={changeValueCustomSelect1}/>
        </div>
    <div>
    <CustomSelect currenscies={currenscies} customSelect={customSelect2} setCustomSelect={changeSelectItem2}/>
        <CustomInput currenscy={currenscy2} setCurrenscy={changeValueCustomSelect2}/>
    </div>
       
    </form>
  )
}

export default CustomForm