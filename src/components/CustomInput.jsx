import React from 'react'

const CustomInput = ({currenscy, setCurrenscy}) => {
  return (
   <span>
       <input type="number" value={currenscy} onChange={e => setCurrenscy(e.target.value)}/>
   </span>
  )
}

export default CustomInput