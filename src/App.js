import React, { useState, useEffect} from "react";
import Header from "./components/Header";
import CustomForm from "./components/CustomForm";
import apiService from "./services/api-service";


function App() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [currenscies, setCurrenscies] = useState({});

  useEffect(
    () => {
      apiService
        .getCurrencyRate()
        .then((currenscyRate) => {
          setCurrenscies(currenscyRate)
          setIsLoaded(true)
        })
    }, []);

  return (
    <div className="App">
      {isLoaded ? 
      <>
        <Header currenscies={currenscies} />
        <CustomForm currenscies={currenscies} />
      </>: 'loading'}
    </div>
  );
}

export default App;
